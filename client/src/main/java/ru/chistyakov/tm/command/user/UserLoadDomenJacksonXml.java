package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.io.IOException;

public class UserLoadDomenJacksonXml extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "uldjx";
    }

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использованием jackson в формате xml";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getUserEndpoint().loadDomainJacksonXml(serviceLocator.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
