package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uupc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля ";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите логи");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("Введите старый пароль");
        final String password = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserEndpoint().findWithLoginPassword(serviceLocator.getSession(),login,password) == null) throw new NullPointerException("Неправильный логин или пароль");
        System.out.println("Введите новый пароль");
        final String newPassword = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserEndpoint().updateUserPassword(serviceLocator.getSession(), login, newPassword)) System.out.println("Пароль изменен");
        else System.out.println("Ошибка изменения пароля");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
