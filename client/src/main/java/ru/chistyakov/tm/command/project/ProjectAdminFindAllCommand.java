package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.Project;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ProjectAdminFindAllCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "pafac";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит все проекты всех пользователей";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        for (Project project : serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSession())) {
            System.out.println("Project{" +
                    "userId='" + project.getUserId() + '\'' +
                    "id='" + project.getId() + '\'' +
                    "name='" + project.getName() + '\'' +
                    ", readinessStatus=" + project.getReadinessStatus() +
                    ", description='" + project.getDescription() + '\'' +
                    ", dateBeginProject=" + project.getDateBeginProject() +
                    ", dateEndProject=" + project.getDateEndProject() +
                    '}');
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
