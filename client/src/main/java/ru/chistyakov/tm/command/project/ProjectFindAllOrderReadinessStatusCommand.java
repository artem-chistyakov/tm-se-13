package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.Project;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ProjectFindAllOrderReadinessStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfarsc";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все проеты в порядке статуса готовности";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        Collection<Project> projectCollection = serviceLocator.getProjectEndpoint().findAllProjectInOrderReadinessStatus(serviceLocator.getSession());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection)
            System.out.println("Project{" +
                    "userId='" + project.getUserId() + '\'' +
                    "id='" + project.getId() + '\'' +
                    "name='" + project.getName() + '\'' +
                    ", readinessStatus=" + project.getReadinessStatus() +
                    ", description='" + project.getDescription() + '\'' +
                    ", dateBeginProject=" + project.getDateBeginProject() +
                    ", dateEndProject=" + project.getDateEndProject() +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
