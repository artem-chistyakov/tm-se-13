package ru.chistyakov.tm.utility;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ISessionRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;
import ru.chistyakov.tm.service.PropertiesService;

import javax.sql.DataSource;

public class SessionFactoryUtil {
    final private PropertiesService propertiesService = new PropertiesService();

    public SqlSessionFactory getSqlSessionFactory() {
        propertiesService.init();
        @Nullable final String user = propertiesService.getJdbcUser();
        @Nullable final String password = propertiesService.getJdbcPassword();
        @Nullable final String url = propertiesService.getJdbcUrl();
        @Nullable final String driver = propertiesService.getJdbcDriver();
        final DataSource dataSource =
                new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
