package ru.chistyakov.tm.utility;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

@UtilityClass
public final class PasswordParser {

    @Nullable
    public static String parse(@Nullable final String password) {
        try {
            if (password == null) return null;
            final byte[] bytesOfMessage = password.getBytes(StandardCharsets.UTF_8);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] thedigest = md.digest(bytesOfMessage);
            return toHexString(thedigest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @NotNull
    private String toHexString(@NotNull byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            final String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }
}
