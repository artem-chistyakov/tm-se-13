package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface IProjectRepository extends IRepository<Project> {

    @Insert("Insert into app_project(id,name,description,date_begin,date_end,user_id,readiness_status) " +
            "values (#{id},#{name},#{description},#{dateBegin},#{dateEnd},#{userId},#{readinessStatus})")
    boolean insert(@NotNull @Param("id") String id, @Param("userId") @NotNull String userId, @Param("name") @NotNull String name,
                   @Param("description") @Nullable String description,
                   @Param("dateBegin") @Nullable Date dateBegin, @Param("dateEnd") @Nullable Date dateEnd,
                   @Param("readinessStatus") @Nullable ReadinessStatus readinessStatus);

    @Insert("Insert into app_project(id,name,description,date_begin,date_end,user_id,readiness_status)" +
            " values (#{id},#{name},#{description},#{dateBeginProject},#{dateEndProject},#{userId},#{readinessStatus})")
    boolean persist(@NotNull Project project);

    @Nullable
    @Select(
            "Select * from app_project where user_id=#{userId} and id=#{projectId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Project findOne(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @NotNull
    @Select("Select * from app_project where user_id=#{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Project> findAllByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("Select * from app_project where user_id=#{userId} order by #{str} asc")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Project> findAllInOrder(@Param("userId") @NotNull String userId, @Param("str") String str);

    @Update("update app_project set name = #{name},description =#{description},date_begin=#{dateBeginProject},date_end=#{dateEndProject} where id =#{id}")
    boolean update(@NotNull Project project);

    @Delete("delete from app_project where user_id = #{userId} and id =#{projectId}")
    boolean remove(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Delete("delete from app_project where user_id = #{userId}")
    void removeAll(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("select * from app_project where user_id=#{userId} and name like #{part} or description like #{part} ")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Project> findByPartNameOrDescription(@Param("userId")String userId,@Param("part") String part);

    @NotNull
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    @Select("select * from app_project")
    Collection<Project> findAll();
}
