package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface ITaskRepository extends IRepository<Task> {
    @Insert("Insert into app_task(id,name,description,date_begin,date_end,user_id,project_id,readiness_status) " +
            "values (#{id},#{taskName},#{descriptionTask},#{dateBeginTask},#{dateEndTask},#{userId},#{projectId},#{readinessStatus})")
    boolean insert(@Param("id") @NotNull String id,
                   @Param("userId") @NotNull String userId,
                   @Param("projectId") @NotNull String projectId,
                   @Param("taskName") @NotNull String taskName,
                   @Param("descriptionTask") @Nullable String descriptionTask,
                   @Param("dateBeginTask") @Nullable Date dateBeginTask,
                   @Param("dateEndTask") @Nullable Date dateEndTask,
                   @Param("readinessStatus") @Nullable ReadinessStatus readinessStatus);

    @Insert("Insert into app_task(id,name,description,date_begin,date_end,user_id,project_id,readiness_status) " +
            "values (#{id},#{name},#{description},#{dateBeginTask},#{dateEndTask},#{userId},#{projectId},#{readinessStatus})")
    boolean persist(@NotNull Task task);

    @Nullable
    @Select(
            "Select * from app_task where user_id=#{userId} and id=#{taskId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Task findOne(@Param("userId") @NotNull String userId, @Param("taskId") @NotNull String taskId);

    @Update("update app_task set name = #{name},description =#{description},date_begin=#{dateBeginTask},date_end=#{dateEndTask} where id =#{id}")
    boolean update(@NotNull Task task);

    @NotNull
    @Select("Select * from app_task where user_id=#{userId} order by #{column} asc")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Task> findAllInOrder(@Param("userId") @NotNull String userId, @Param("part") @NotNull String column);

    @Delete("delete from app_task where user_id = #{userId} and id =#{taskId}")
    boolean remove(@Param("userId") @NotNull String userId, @Param("taskId") @NotNull String taskId);

    @Delete("delete from app_task where user_id =#{userId} and project_id =#{projectId}")
    boolean removeByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Delete("delete from app_task where user_id = #{userId}")
    void removeAll(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("Select * from app_task where user_id=#{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Task> findAllByUserId(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("select * from app_task where user_id=#{userId} and name like #{part} or description like #{part} ")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Task> findByPartNameOrDescription(@Param("userId") @NotNull String userId, @Param("part") @Nullable String part);

    @Select("select * from app_task")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateBeginProject", column = "date_begin"),
            @Result(property = "dateEndProject", column = "date_end"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "readinessStatus", column = "readiness_status")
    })
    Collection<Task> findAll();
}
