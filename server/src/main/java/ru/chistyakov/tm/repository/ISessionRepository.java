package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;

import java.sql.SQLException;

public interface ISessionRepository {

    @Insert("insert into app_session(id,signature,timestamp,role,user_id)values(#{id},#{signature},#{timestamp},#{roleType},#{userId})")
    boolean create(@Nullable Session session);

    @Select("select * from app_session where id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "userId", column = "user_id")
    })
    Session contains(@Param ("id")@Nullable String id);

    @Delete("delete from app_user where id =#{id}")
    boolean delete(@Param ("id")@Nullable String id);
}
