package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.entity.AbstractEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;


public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull
    final Map<String, T> entities = new LinkedHashMap<>();

    public abstract boolean persist(T t) throws SQLException;

    public abstract @Nullable boolean merge(T t) throws SQLException;

}
