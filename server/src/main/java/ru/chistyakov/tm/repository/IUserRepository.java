package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Select(
            "Select * from app_user where id=#{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password_hash"),
            @Result(property = "roleType", column = "role")
    })
    User findOne(@Param("id") @NotNull String id);

    @Delete("delete from app_project where id = #{id}")
    boolean remove(@Param("id") @NotNull String id);

    @Insert("Insert into app_user(id,login,password_hash,role)" +
            " values (#{id},#{login},#{password},#{roleType})")
    boolean persist(@NotNull User user);

    @Update("update app_user set login = #{login},password_hash =#{password},role=#{roleType}")
    boolean update(@NotNull User user);

    @Select(
            "Select * from app_user where login = #{login} and password_hash =#{password} limit 1 ")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password_hash"),
            @Result(property = "roleType", column = "role")
    })
    User findOneWithLoginPassword(@Param("login") @Nullable String login, @Param("password") @Nullable String password);

    @Insert("Insert into app_user(id,login,password_hash,role) " +
            "values (#{id},#{login},#{password},#{roleType})")
    boolean insert(@Param("id") @NotNull String id, @Param("login") @Nullable String login,
                   @Param("password") @Nullable String password, @Param("roleType") @Nullable RoleType roleType);

    @NotNull
    @Select("Select * from app_user")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password_hash"),
            @Result(property = "roleType", column = "role")
    })
    Collection<User> findAll();
}
