package ru.chistyakov.tm.enumerate;

public enum ReadinessStatus {

    PLANNED("запланированно"), PROCESSING("в процессе"), READY("готово");

    private final String name;

    ReadinessStatus(String name) {
        this.name = name;
    }


    public String displayName() {
        return name;
    }
}
