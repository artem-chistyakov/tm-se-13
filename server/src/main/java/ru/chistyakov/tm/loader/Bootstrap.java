package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.endpoint.ProjectEndpoint;
import ru.chistyakov.tm.endpoint.SessionEndpoint;
import ru.chistyakov.tm.endpoint.TaskEndpoint;
import ru.chistyakov.tm.endpoint.UserEndpoint;
import ru.chistyakov.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private PropertiesService propertiesService = new PropertiesService();

    @NotNull
    private final IProjectService projectService = new ProjectService();
    @NotNull
    private final IDomainService domainService = new DomainService();
    @NotNull
    private final ITaskService taskService = new TaskService();
    @NotNull
    private final IUserService userService = new UserService();
    @NotNull
    private final ISessionService sessionService = new SessionService( propertiesService);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(userService, sessionService, domainService);

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    private void init() {
        propertiesService.init();
        Endpoint.publish(projectEndpoint.getURL(), projectEndpoint);
        Endpoint.publish(taskEndpoint.getURL(), taskEndpoint);
        Endpoint.publish(userEndpoint.getURL(), userEndpoint);
        Endpoint.publish(sessionEndpoint.getURL(), sessionEndpoint);
    }

    public void start() {
        init();
    }
}

