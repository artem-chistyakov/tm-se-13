package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.util.Comparator;

public final class TaskReadinessStatusComparator implements Comparator<Task> {
    @Override
    public int compare(@Nullable final Task o1, @Nullable final Task o2) {
        if (o1 == null || o2 == null) throw new NullPointerException();
        int resultComparing = o1.getReadinessStatus().compareTo(o2.getReadinessStatus());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}

