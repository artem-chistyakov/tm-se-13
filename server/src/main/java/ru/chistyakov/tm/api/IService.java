package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Collection;

public interface IService<T> {

    boolean merge(T t);

    boolean persist(T t);

    @Nullable
    Collection<T> findAll() ;
}
