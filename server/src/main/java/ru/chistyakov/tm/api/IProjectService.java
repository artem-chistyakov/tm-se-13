package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectService extends IService<Project> {

    @WebMethod
    boolean insert(@Nullable String userId, @Nullable String name, @Nullable String descriptionProject,
                   @Nullable String dateBegin, @Nullable String dateEnd);

    @WebMethod
    boolean merge(@Nullable Project project);

    @WebMethod
    void removeAll(@Nullable String userId);

    @WebMethod
    boolean persist(@Nullable Project project);

    @WebMethod
    boolean remove(@Nullable String userId, @Nullable String projectId);

    @WebMethod
    @Nullable
    Project findOne(@Nullable String userId, @Nullable String projectId);

    @WebMethod
    boolean update(@Nullable String projectId, @Nullable String projectName, @Nullable String descriptionProject, @Nullable String dateBegin, @Nullable String dateEnd,
                   @Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllByUserId(String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllInOrderDateBegin(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllInOrderDateEnd(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllInOrderReadinessStatus(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findByPartNameOrDescription(@Nullable String userId, String part);

}