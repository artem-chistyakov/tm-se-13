package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    User authoriseUser(@Nullable String name, @Nullable String password);

    boolean updateUser(@Nullable String userId, @Nullable String login, @Nullable String password);

    boolean updatePassword(@Nullable String userId, @Nullable String login, @Nullable String password);

    User findWithLoginPassword(@Nullable String login, @Nullable String password);

    boolean registryAdmin(@Nullable String login, @Nullable String password);

    boolean registryUser(@Nullable String login, @Nullable String password);

    User findById(@Nullable final String userId);
}
