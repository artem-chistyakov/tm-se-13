package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    @NotNull
    String getURL();

    @WebMethod
    boolean registryUser(@Nullable @WebParam(name = "login", partName = "login") String login,
                      @Nullable @WebParam(name = "password", partName = "password") String password);

    @WebMethod
    Session authorizeUser(@Nullable @WebParam(name = "login", partName = "login") String login,
                          @Nullable @WebParam(name = "password", partName = "password") String password);

    @WebMethod
    @Nullable boolean updateUser(@Nullable @WebParam(name = "session", partName = "session") Session session,
                              @Nullable @WebParam(name = "login", partName = "login") String login,
                              @Nullable @WebParam(name = "password", partName = "password") String password);

    @WebMethod
    @Nullable boolean persistUser(@Nullable @WebParam(name = "user", partName = "user") User user);

    @WebMethod
    @Nullable boolean updateUserPassword(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                      @Nullable @WebParam(name = "login", partName = "login") String login,
                                      @Nullable @WebParam(name = "password", partName = "password") String password);

    @WebMethod
    @Nullable
    User findWithLoginPassword(  @Nullable @WebParam(name = "session", partName = "session") Session session,
                                 @Nullable @WebParam(name = "login", partName = "login") String login,
                                 @Nullable @WebParam(name = "password", partName = "password") String password);
    @WebMethod
    @Nullable User findUserById(@Nullable @WebParam(name = "session", partName = "session") Session session);
    @WebMethod
    @Nullable Collection<User> findAllUser(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable boolean registryAdmin(@Nullable @WebParam(name = "login", partName = "login") String login,
                                 @Nullable @WebParam(name = "password", partName = "password") String password);

    @WebMethod
    void serializateDomain(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    void deserializateDomain(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    void saveDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    void loadDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    void saveDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    void loadDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") Session session);
}
