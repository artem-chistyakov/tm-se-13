package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

public interface ISessionService {


    void validate(@Nullable Session session) throws IllegalAccessException;

    boolean isValid(@Nullable Session session);

    Session open(@Nullable String userId, RoleType roleType);

    boolean close(@Nullable Session session);
}
