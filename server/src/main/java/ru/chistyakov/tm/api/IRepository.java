package ru.chistyakov.tm.api;

import ru.chistyakov.tm.entity.AbstractEntity;

import java.sql.SQLException;

public interface IRepository<T extends AbstractEntity> {

    boolean persist(T t) throws SQLException;

    boolean merge(T t) throws SQLException;
}
