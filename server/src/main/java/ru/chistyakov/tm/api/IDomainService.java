package ru.chistyakov.tm.api;

public interface IDomainService {
    void serializateDomain();

    void deserializateDomain();

    void saveDomainJacksonXml();

    void loadDomainJacksonXml();

    void saveDomainJacksonJson();

    void loadDomainJacksonJson();
}
