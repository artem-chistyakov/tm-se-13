package ru.chistyakov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "user")
public final class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";
    @NotNull
    private String password = "";
    @NotNull
    private RoleType roleType = RoleType.USUAL_USER;
}
