package ru.chistyakov.tm.endpoint;


import lombok.Getter;

public abstract class AbstractEndpoint {
    @Getter
    String URL;
}
