package ru.chistyakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.ReadinessStatus;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.utility.DateParser;
import ru.chistyakov.tm.utility.SessionFactoryUtil;

import java.util.Collection;
import java.util.UUID;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final SessionFactoryUtil sessionFactoryUtil = new SessionFactoryUtil();
    private final SqlSessionFactory sessionFactory = sessionFactoryUtil.getSqlSessionFactory();

    @Override
    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findOne(project.getUserId(), project.getId()) == null) {
                final boolean a = projectRepository.persist(project);
                if (a) session.commit();
                else session.rollback();
                return a;
            } else {
                final boolean b = projectRepository.update(project);
                if (b) session.commit();
                else session.rollback();
                return b;
            }
        }
    }

    @Override
    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.persist(project)) {
                session.commit();
                return true;
            } else session.rollback();
            return false;
        }
    }

    @Override
    public boolean insert(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String descriptionProject, @Nullable final String dateBeginProject,
                          @Nullable final String dateEndProject) {
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.insert(UUID.randomUUID().toString(), userId, name, descriptionProject,
                    DateParser.parseDate(simpleDateFormat, dateBeginProject),
                    DateParser.parseDate(simpleDateFormat, dateEndProject),
                    ReadinessStatus.PLANNED)) {
                session.commit();
                return true;
            } else session.rollback();
            return false;
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.trim().isEmpty()) return;
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.removeAll(userId);
            session.commit();
        }
    }

    @Override
    public boolean remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return false;
        if (projectId.trim().isEmpty()) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.remove(userId, projectId)) {
                session.commit();
                return true;
            } else {
                session.rollback();
                return false;
            }
        }
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            return projectRepository.findOne(userId, projectId);
        }
    }

    @Override
    public boolean update(@Nullable final String projectId, @Nullable final String projectName,
                          @Nullable final String descriptionProject, @Nullable final String dateBegin,
                          @Nullable final String dateEnd, @Nullable final String userId) {
        final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(descriptionProject);
        if (dateBegin != null) project.setDateBeginProject(DateParser.parseDate(simpleDateFormat, dateBegin));
        if (dateEnd != null) project.setDateEndProject(DateParser.parseDate(simpleDateFormat, dateEnd));
        try (final SqlSession session = sessionFactory.openSession()) {
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findOne(userId, projectId) == null) return false;
            if (projectRepository.update(project)) {
                session.commit();
                return true;
            } else {
                session.rollback();
                return false;
            }
        }
    }


    @Nullable
    @Override
    public Collection<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllByUserId(userId);
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllInOrder(userId, "date_begin");
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllInOrder(userId, "date_end");
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllInOrder(userId, "readiness_status");
        }
    }

    @Nullable
    @Override
    public Collection<Project> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findByPartNameOrDescription(userId, part);
        }
    }

    @Nullable
    public Collection<Project> findAll() {
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll();
        }
    }
}
