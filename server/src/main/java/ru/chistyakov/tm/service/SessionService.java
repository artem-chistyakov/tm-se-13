package ru.chistyakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.repository.ISessionRepository;
import ru.chistyakov.tm.utility.SessionFactoryUtil;
import ru.chistyakov.tm.utility.SignatureUtil;

public final class SessionService implements ISessionService {

    private final SessionFactoryUtil sessionFactoryUtil = new SessionFactoryUtil();
    private final SqlSessionFactory sessionFactory = sessionFactoryUtil.getSqlSessionFactory();

    @NotNull
    private final PropertiesService propertiesService;

    public SessionService(@NotNull final PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    @Override
    public void validate(@Nullable final Session session) throws IllegalAccessException {
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (session == null) throw new IllegalAccessException();
            if (session.getUserId() == null || session.getUserId().isEmpty()) throw new IllegalAccessException();
            if (session.getTimestamp() == null) throw new IllegalAccessException();
            if (session.getSignature() == null || session.getSignature().isEmpty()) throw new IllegalAccessException();
            final String signatureSource = session.getSignature();
            Session session1 = (Session) session.clone();
            if (propertiesService.getSALT() == null || propertiesService.getCycle() == null)
                throw new IllegalArgumentException("Ошибка загрузки проперти файла");
            final String signatureTarget = SignatureUtil.sign(session1, propertiesService.getSALT(), propertiesService.getCycle());
            final boolean check = signatureSource.equals(signatureTarget);
            if (!check) throw new IllegalAccessException();
            if (sessionRepository.contains(session.getId()) == null) throw new IllegalAccessException();
        }
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Session open(@Nullable final String userId, RoleType roleType) {
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (userId == null || userId.isEmpty()) return null;
            final Session session = new Session();
            session.setUserId(userId);
            session.setRoleType(roleType);
            final String signature = SignatureUtil.sign(session, propertiesService.getSALT(), propertiesService.getCycle());
            session.setSignature(signature);
            sessionRepository.create(session);
            sqlSession.commit();
            final Session openedSession = sessionRepository.contains(session.getId());
            return openedSession;

        }
    }

    @Override
    public boolean close(@Nullable final Session session) {
        if (session == null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (sessionRepository.delete(session.getId())) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }
}
