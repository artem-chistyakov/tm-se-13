package ru.chistyakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.repository.IUserRepository;
import ru.chistyakov.tm.utility.PasswordParser;
import ru.chistyakov.tm.utility.SessionFactoryUtil;

import java.util.Collection;
import java.util.UUID;

public final class UserService extends AbstractService<User> implements IUserService {

    private final SessionFactoryUtil sessionFactoryUtil = new SessionFactoryUtil();
    private final SqlSessionFactory sessionFactory = sessionFactoryUtil.getSqlSessionFactory();

    @Override
    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.findOne(user.getId()) == null) {
                final boolean a = userRepository.persist(user);
                if (a) sqlSession.commit();
                else sqlSession.rollback();
                return a;
            } else {
                final boolean b = userRepository.update(user);
                if (b) sqlSession.commit();
                else sqlSession.rollback();
                return b;
            }
        }
    }

    @Override
    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.persist(user)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    @Nullable
    public User authoriseUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneWithLoginPassword(login, passwordMD5);
        }
    }

    @Override
    public boolean updateUser(@Nullable final String userId, @Nullable final String login,
                              @Nullable final String password) {
        if (userId == null || login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            final User user = userRepository.findOne(userId);
            user.setPassword(passwordMD5);
            user.setLogin(login);
            if (userRepository.update(user)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean updatePassword(@Nullable final String userId, @Nullable final String login,
                                  @Nullable final String password) {
        if (userId == null || login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            final User user = userRepository.findOne(userId);
            user.setPassword(passwordMD5);
            if (userRepository.update(user)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    @Nullable
    public User findWithLoginPassword(@Nullable String login, @Nullable String password) {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if(passwordMD5 == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneWithLoginPassword(login, passwordMD5);
        }
    }


    @Override
    public boolean registryAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.findOneWithLoginPassword(login, passwordMD5) != null) return false;
            if (userRepository.insert(UUID.randomUUID().toString(), login, passwordMD5, RoleType.ADMINISTRATOR)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean registryUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        if (findWithLoginPassword(login, passwordMD5) != null) return false;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.findOneWithLoginPassword(login, passwordMD5) != null) return false;
            if (userRepository.insert(UUID.randomUUID().toString(), login, passwordMD5, RoleType.USUAL_USER)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }


    @Override
    @Nullable
    public User findById(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOne(userId);
        }
    }

    @Override
    @Nullable
    public Collection<User> findAll() {
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }
}
