package ru.chistyakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.ReadinessStatus;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.utility.DateParser;
import ru.chistyakov.tm.utility.SessionFactoryUtil;

import java.util.Collection;
import java.util.UUID;


public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final SessionFactoryUtil sessionFactoryUtil = new SessionFactoryUtil();
    private final SqlSessionFactory sessionFactory = sessionFactoryUtil.getSqlSessionFactory();

    @Override
    public boolean merge(@Nullable final Task task) {
        if (task == null || task.getUserId() == null) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository.findOne(task.getUserId(), task.getId()) == null) {
                final boolean a = taskRepository.persist(task);
                if (a) session.commit();
                else session.rollback();
                return a;
            } else {
                final boolean b = taskRepository.update(task);
                if (b) session.commit();
                else session.rollback();
                return b;
            }
        }
    }

    @Override
    public boolean persist(@Nullable final Task task) {
        if (task == null) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository.persist(task)) {
                session.commit();
                return true;
            } else session.rollback();
            return false;
        }
    }

    @Override
    public boolean insert(@Nullable final String userId, @Nullable final String projectId,
                          @Nullable final String taskName, @Nullable final String descriptionTask,
                          @Nullable final String dateBeginTask, @Nullable final String dateEndTask) {
        if (userId == null || projectId == null || taskName == null) return false;
        if (userId.trim().isEmpty()) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findOne(userId, projectId) == null) return false;
            if (taskRepository.insert(UUID.randomUUID().toString(), userId, projectId, taskName, descriptionTask,
                    DateParser.parseDate(simpleDateFormat, dateBeginTask),
                    DateParser.parseDate(simpleDateFormat, dateEndTask),
                    ReadinessStatus.PLANNED)) {
                session.commit();
                return true;
            } else session.rollback();
            return false;
        }
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return null;
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return null;
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            return taskRepository.findOne(userId, taskId);
        }
    }

    @Nullable
    @Override
    public Collection<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByUserId(userId);
        }
    }

    @Override
    public boolean update(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId,
                          @Nullable final String taskName, @Nullable final String description, @Nullable final String dateBegin,
                          @Nullable final String dateEnd) {
        if (userId == null || taskId == null || projectId == null || taskName == null) return false;
        if (taskId.trim().isEmpty() || taskName.trim().isEmpty()) return false;
        final Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(description);
        task.setProjectId(projectId);
        if (dateBegin != null) task.setDateBeginTask(DateParser.parseDate(simpleDateFormat, dateBegin));
        if (dateEnd != null) task.setDateEndTask(DateParser.parseDate(simpleDateFormat, dateEnd));
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository.findOne(userId, taskId) == null) return false;
            if (taskRepository.update(task)) {
                session.commit();
                return true;
            } else {
                session.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return false;
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return false;
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository.remove(userId, taskId)) {
                session.commit();
                return true;
            } else {
                session.rollback();
                return false;
            }
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        try (final SqlSession session = sessionFactory.openSession()) {
            final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAll(userId);
            session.commit();
        }
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllInOrder(userId, "date_begin");
        }
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllInOrder(userId, "date_end");
        }
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllInOrder(userId, "readiness_status");
        }
    }

    @Override
    public @Nullable Collection<Task> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findByPartNameOrDescription(userId, part);
        }
    }

    @NotNull
    public Collection<Task> findAll() {
        try (final SqlSession sqlSession = sessionFactory.openSession()) {
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll();
        }
    }
}
